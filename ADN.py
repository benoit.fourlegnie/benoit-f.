import random

#QUESTION 1

def estADN(chaine):
    '''
 vérifie si la chaîne de caractères
passée en paramètre ne contient aucun autre caractère que les quatre bases
A, C, G et T.
    :param u: (str) chaine de carctère
    :return: (boll) True False

'''
    res=True
    for i in chaine:
        if i not in ['A','C','G','T']:
            res=False
    return res

print(estADN('ATGCGATC'))
print(estADN('ACKT'))
print(estADN(""))

# QUESTION 2


def genereADN(n):
    '''
générer aléatoirement une séquence ADN
'''
    liste=['A','C','G','T']
    chaine=""
    for i in range(n):
        nb=random.randint(0,3)
        chaine=chaine+liste[nb]
    return chaine

print(genereADN(10))


# question 3

def baseComplementaire(base,type):
    dicoARN={'A':'U','T':'A','G':'C','C':'G'}
    dicoADN={'U':'A','A':'T','C':'G','G':'C'}
    if type=="ADN":
        return dicoADN[base]
    if type=="ARN":
        return dicoARN[base] 
 

print(baseComplementaire('G', 'ADN'))    
print(baseComplementaire('A', 'ARN'))

# question 4

def transcrit(chaine,debut,fin):
    res=""
    for i in chaine[debut-1:fin]:
        res=res+baseComplementaire(i,"ARN")
    return res
print(transcrit('TTCTTCTTCGTACTTTGTGCTGGCCTCCACACGATAATCC', 4, 23))


# question 5/6

def traduit(ADN):
    proteine=""
    for i in range(0,len(ADN),3):
        codon=ADN[i:i+3]
        if codon=='UUU' or codon=='UUC':
            proteine=proteine+'F'
        if codon=='UUA' or codon=='UUG' or codon =='CUU'or codon=='CUC' or codon=='CUA' or codon== 'CUG' :
            proteine=proteine+'L'
        if codon=='AUU'or codon=='AUC' or codon=='AUA' :
            proteine=proteine+'I'
        if  codon=='AUG' :
            proteine=proteine+'M'
        if  codon=='GUU'or codon== 'GUC'or codon=='GUA'or codon=='GUG' :
            proteine=proteine+'V'
        if  codon=='UCU'or codon=='UCC'or codon=='UCA' or codon== 'UCG' or codon=='AGU'or codon== 'AGC' :
            proteine=proteine+'S'
        if  codon=='CCU' or codon=='CCC' or codon=='CCA' or codon=='CCG' :
            proteine=proteine+'P'
        if  codon=='ACU'or codon=='ACC'or codon=='ACA'or codon=='ACG' :
            proteine=proteine+'T'
        if codon=='GCU'or codon=='GCC'or codon=='GCA'or codon=='GCG' :
            proteine=proteine+'A'
        if codon=='UAU' or codon=='UAC' :
            proteine=proteine+'Y'
        if  codon=='UAA'or codon=='UAG'or codon=='UGA' :
            proteine=proteine+'*'
        if  codon=='CAU' or codon=='CAC' :
            proteine=proteine+'H'
        if codon=='CAA'or codon=='CAG' :
            proteine=proteine+'Q'
        if codon=='AAU'or codon=='AAC' :
           proteine=proteine+'N'
        if codon=='AAA' or codon=='AAG' :
            proteine=proteine+'K'
        if codon=='GAU' or codon=='GAC' :
            proteine=proteine+'D'
        if  codon=='GAA' or codon=='GAG' :
            proteine=proteine+'E'
        if  codon=='UGU'or codon=='UGC' :
            proteine=proteine+'C'
        if  codon=='UGG' :
            proteine=proteine+'W'
        if codon=='CGU'or codon=='CGC'or codon=='CGA'or codon=='CGG'or codon=='AGA'or codon=='AGG' :
            proteine=proteine+'R'
        if  codon=='GGU' or codon=='GGC' or codon=='GGA' or codon=='GGG' :
            proteine=proteine+'G'
    return proteine

print(traduit('AUGCGAAGCCGAAAGAACACCGGCUAA'))

#question 
