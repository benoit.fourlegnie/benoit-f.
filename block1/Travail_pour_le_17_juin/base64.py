BASE64_SYMBOLS = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H',
                  'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P',
                  'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X',
                  'Y', 'Z', 'a', 'b', 'c', 'd', 'e', 'f',
                  'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n',
                  'o', 'p', 'q', 'r', 's', 't', 'u', 'v',
                  'w', 'x', 'y', 'z', '0', '1', '2', '3',
                  '4', '5', '6', '7', '8', '9', '+', '/']



def to_base64(triplet):
    '''
    convertit le triplet d'octets en une chaîne de quatre symboles
    
    :param triplet: (tuple ou list) une séquence d'octets
    :return: (str) la chaîne de symboles de la base 64 représentant le triplet d'octets
    :CU: 1 <= len(triplet) <= 3 et les entiers de triplet tous compris entre 0 et 255
    :Exemple:
    
    >>> to_base64((18, 184, 156))
    'Eric'
    >>> to_base64((18, 184))
    'Erg='
    >>> to_base64((18,))
    'Eg=='
    '''
    if len(triplet)==1:
        char1=BASE64_SYMBOLS[triplet[0] >>2]
        char2=BASE64_SYMBOLS[(triplet[0] & 3) <<4]
        char3="="
        char4="="
    if len(triplet)==2:
        char1=BASE64_SYMBOLS[triplet[0] >>2]
        char2=BASE64_SYMBOLS[(triplet[0] & 3) <<4  | triplet[1] >> 4]
        char3=BASE64_SYMBOLS[(triplet[1] & 15) <<2]
        char4="="
    if len(triplet)==3:
        char1=BASE64_SYMBOLS[triplet[0] >>2]
        char2=BASE64_SYMBOLS[(triplet[0] & 3) <<4  | triplet[1] >> 4]
        char3=BASE64_SYMBOLS[(triplet[1] & 15) <<2 | triplet[2] >> 6]
        char4=BASE64_SYMBOLS[triplet[2] & 63]
    return char1 + char2 + char3 + char4


def from_base64(b64_string):
    '''
    convertit une chaîne de quatre symboles en un tuple (le plus souvent triplet) d'octets
    
    :param b64_string: (str) une chaîne de symboles de la base 64
    :return: (tuple) un tuple d'octets dont b64_string est la représentation en base 64
    :CU: len(b64_string) == 4 et les caractères de b64_string sont dans la table ou le symbole =
    :Exemple:
    
    >>> from_base64('Eric')
    (18, 184, 156)
    >>> from_base64('Erg=')
    (18, 184)
    >>> from_base64('Eg==')
    (18,)
    '''
    char0=BASE64_SYMBOLS.index(b64_string[0])
    char1=BASE64_SYMBOLS.index(b64_string[1])
    char2=BASE64_SYMBOLS.index(b64_string[2])
    char3=BASE64_SYMBOLS.index(b64_string[3])
    return (char0 << 2 | char1 >>4 , char1 & 15 | char2 >>2 , char2 & 3 | char3)