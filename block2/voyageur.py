from TSP_biblio import *
import numpy as np

tour=get_tour_fichier("exemple.txt")



def genere_matrice(tour):
    distancier=np.eye(len(tour),len(tour))
    for ville1 in range(len(tour)):
        for ville2 in range(len(tour)):
                distancier[ville1,ville2]=distance([tour[ville1],tour[ville2]],0,1)
    return distancier

distancier=genere_matrice(tour)

            
            
def ville_la_plus_proche(numero_ville,liste_ville):
    ListeDistance=[]
    for ville in liste_ville:
        if ville != numero_ville:
            ListeDistance.append([ville,distancier[numero_ville,ville]])
    distance_min=ListeDistance[1][1]
    indice=ListeDistance[1][0]
    for couple in  ListeDistance:
        if couple[1]<distance_min:
            distance_min=couple[1]
            indice=couple[0]
    return [distance_min,indice]



def supprimer(liste,elt):
    tmp=[]
    for i in liste:
        if not(i==elt) :
            tmp.append(i)
    return tmp
        



def voyageur(ville_depart):
    tour=get_tour_fichier("exemple.txt")
    distancier=genere_matrice(tour)
    liste=range(len(tour))
    distance=[]
    ville=[ville_depart]
    numero_ville=ville_depart
    liste=supprimer(liste,ville_depart)
    for i in range(len(tour)-2):
            tmp=ville_la_plus_proche(numero_ville,liste)
            ville.append(tmp[1])
            distance.append(tmp[0])
            numero_ville=tmp[1]
            liste=supprimer(liste,tmp[1])
    return [ville, sum(distance)]





'''
distance=voyageur(2)[1]
print(distance)
optimisation=[0,distance]
for i in range(3):
    print(i)
'''
dist_min=voyageur(0)[1]
ville_dep=0
for i in range(len(tour)):
    if voyageur(i)[1] <dist_min:
        dist_min=voyageur(i)[1]
        ville_dep=i
print(dist_min)

parcours=voyageur(ville_dep)[1]
liste=[]
for elt in voyageur(ville_dep)[0]:
    liste.append([tour[elt][0],tour[elt][1],tour[elt][2]])
    
print(tour[ville_dep])
trace(liste)

