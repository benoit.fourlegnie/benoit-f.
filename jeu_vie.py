import random
import time

#def creer_grille(ligne,colonne):
#    return[[0]*colonne]*ligne # 

# phenomene bizarre !!
#grille=creer_grille(2,3)
#print(grille)
#grille1[0][0]=12
#print(grille1)

def creer_grille(ligne,colonne):
    grille=[]
    MaLigne=[]
    for i in range(ligne):
        MaLigne.append(0)
    for j in range(colonne):
        MaLigneBis=MaLigne.copy()
        grille.append(MaLigneBis)
    return grille

def hauteur_grille(grille):
    return len(grille)

def largeur_grille(grille):
    return len(grille[0])

print(hauteur_grille(creer_grille(3, 2)))
print(largeur_grille(creer_grille(3, 2)))

def creer_grille_aleatoire(ligne,colonne,proba):
    grille=creer_grille(ligne,colonne)
    for i in range(ligne):
        for j in range(colonne):
            alea=random.random()
            if alea<proba:
                grille[i][j]=1
    return grille

print(creer_grille_aleatoire(4,4,0.5))


def voisins_case(grille,colonne,ligne):
    liste=[]
    largeur=largeur_grille(grille)
    hauteur=hauteur_grille(grille)
    j=colonne
    i=ligne
    if i>=1 and i<=largeur-2 and j>=1 and j<=hauteur-2:
        liste=[grille[i-1][j-1],grille[i-1][j],grille[i-1][j+1],grille[i][j-1],grille[i+1][j],grille[i+1][j-1],grille[i][j+1],grille[i+1][j+1]]
    if (j,i)==(0,0):
        return [grille[0][1],grille[1][0],grille[1][1]]
    if (j,i)==(largeur-1,hauteur-1):
        return [grille[hauteur-2][largeur-2],grille[hauteur-2][largeur-1],grille[hauteur-1][hauteur-1]]
    if (j,i)==(0,hauteur-1):
        return [grille[hauteur-2][0],grille[hauteur-2][1],grille[hauteur-1][1]]
    if (j,i)==(largeur-1,0):
        return [grille[largeur-2][0],grille[largeur-2][1],grille[largeur-1][1]]
    if j==0 and i>=1 and i<=hauteur-2:
        return [grille[i-1][0],grille[i-1][1],grille[i][1],grille[i+1][1],grille[i+1][0]]
    if i==hauteur-1 and j>=1 and j<=largeur-2:
        return [grille[i][j-1],grille[i][j+1],grille[i-1][j-1],grille[i-1][j],grille[i-1][j+1]]
    if i==0 and j>=1 and j<=largeur-2:
        return [grille[i][j-1],grille[i][j+1],grille[i+1][j-1],grille[i+1][j],grille[i+1][j+1]]
    if j==largeur-1 and i>=1 and i<=hauteur-2:
        return [grille[i-1][largeur-1],grille[i-1][largeur-2],grille[i][largeur-2],grille[i+1][largeur-2],grille[i+1][largeur-1]]
    return liste

def nb_cellules_voisins(grille,colonne,ligne):
    return sum(voisins_case(grille,colonne,ligne))


def afficher_grille(grille):
    largeur=largeur_grille(grille)
    hauteur=hauteur_grille(grille)
    for j in range(hauteur):
        tmp=""
        for i in range(largeur):
            if grille[j][i]==1:
                tmp=tmp+"o "
            else:
                tmp=tmp+"_ "
        print(tmp)
def generation_suivante(grille):
    largeur=largeur_grille(grille)
    hauteur=hauteur_grille(grille)
    grille_tempo=creer_grille(largeur,hauteur)
    for j in range(hauteur):
        for i in range(largeur):
            if nb_cellules_voisins(grille,i,j)==3:
                grille_tempo[j][i]=1
            if nb_cellules_voisins(grille,i,j)<2 or nb_cellules_voisins(grille,i,j)>3 :
                grille_tempo[j][i]=0
            if nb_cellules_voisins(grille,i,j)==2:
                grille_tempo[j][i]=grille[j][i]
    return grille_tempo

grille = [[0, 1, 0], [1, 0, 0], [1, 1, 1]]
            
def evolution_n_generations(grille,n):
    for i in range(n):
        grille=generation_suivante(grille)
        afficher_grille(grille)
        time.sleep(0.25)
        
grille=[[0, 0, 1, 0], [1, 0, 0, 1], [1, 0, 0, 1], [0, 1, 0, 0]]
grille=[[0, 1, 0, 0, 0], [0, 0, 1, 0, 0], [1, 1, 1, 0, 0], [0, 0, 0, 0, 0], [0, 0, 0, 0, 0]]
print(evolution_n_generations(grille,11))