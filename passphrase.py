from liste_7776_mots import LISTE_MOTS

print(type(LISTE_MOTS))

print(len(LISTE_MOTS))
      
def verif_all_string(liste):
    '''
    Vérifie que tous les élements de la liste sont des chaines de caractères
    :param liste: (list)
    :return: boolean
    :CU: aucune
    
    >>>verif_all_string(LISTE_MOTS)
    True
    '''
    return all(isinstance(elt,str) for elt in liste)


def est_mot(mot):
    '''
    Vérifie que mot ne contient que des caractères
    :param liste: (str)
    :return: boolean
    :CU: aucune
    '''

    return(all(ord(caractere)>=65 and ord(caractere)<=122 for caractere in mot))


def est_liste_mot(liste):
    '''
    Vérifie que chaque mot de  la liste ne contiennent que des caractères
    :param liste: liste
    :return: boolean
    :CU: aucune
    '''
    
    return(all(est_mot(mot) for mot in liste))


def tous_differents(liste):
    '''
    Vérifie que la liste contient des mots tous différents
    :param liste: liste
    :return: boolean
    :CU: aucune
    '''
    return len(set(liste))==len(liste)
    
    
def long_min(liste):
    liste_longueur=[]
    for elt in liste:
        liste_longueur.append(len(elt))
    return min(liste_longueur)

'''
return min(set(len(elt) for elt in liste))
'''


def long_max(liste):
    liste_longueur=[]
    for elt in liste:
        liste_longueur.append(len(elt))
    return max(liste_longueur)
'''
return max(set(len(elt) for elt in liste))
'''

def imprime(liste):
    print("0 : "+ liste[0])
    print(str(len(liste)) + " : " + liste[len(liste)-1])
    print("2094 : "+ liste[2094])


def en_nombre(sequence):
    return sum((int(sequence[k])-1)*6**(4-k) for k in range(5))

def donne_mot(sequence):
    return LISTE_MOTS[sum((int(sequence[k])-1)*6**(4-k) for k in range(5))]
    