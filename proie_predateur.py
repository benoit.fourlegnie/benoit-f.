import random
import numpy
from matplotlib import pylab


initialisation_thon=30.0
initialisation_requin=10.0
gestation_thon=2
gestation_requin=4
energie=3


def creer_mer(taille):
    grille=[]
    MaLigne=[]
    for i in range(taille):
        MaLigne.append(0)
    for j in range(taille):
        #MaLigneBis=MaLigne.copy()
        MaLigneBis=list(MaLigne)
        grille.append(MaLigneBis)
    return grille

# chaque case de la mer est constitué de la liste suivante:
#[[présent,gestation thon],[présent, gestation,energie]
# présent 0 ou 1


# céeer une mer avec 30% de thon, 10% de requin et 60% de case vide.


def remplissage_mer(taille,thon,requin):
    mer=creer_mer(taille)
    for i in range(taille):
        for j in range(taille):
            alea=random.random()
            if alea<=initialisation_requin/100.0:
                mer[i][j]=[[0,gestation_thon],[1,gestation_requin,energie]]
            if alea > initialisation_requin/100 and alea<=initialisation_requin/100+initialisation_thon/100:
                mer[i][j]=[[1,gestation_thon],[0,gestation_requin,energie]]
            if alea>initialisation_requin/100+initialisation_thon/100:
                mer[i][j]=[[0,gestation_thon],[0,gestation_requin,energie]]
    return mer



def est_libre(mer,x,y):
    return  mer[y][x][0][0]==0 and mer[y][x][1][0]==0
        
            
def liste_cases_voisines_libre(mer,x,y):
    liste=[]
    taille=len(mer)
    if est_libre(mer,(x-1)%taille,(y+1)%taille) :
        liste.append(((x-1)%taille,(y+1)%taille))
    if est_libre(mer,(x)%taille,(y+1)%taille) :
        liste.append(((x)%taille,(y+1)%taille))
    if est_libre(mer,(x+1)%taille,(y+1)%taille) :
        liste.append(((x+1)%taille,(y+1)%taille))
    if est_libre(mer,(x-1)%taille,(y)%taille) :
        liste.append(((x-1)%taille,(y)%taille))
    if est_libre(mer,(x+1)%taille,(y)%taille) :
        liste.append(((x+1)%taille,(y)%taille))
    if est_libre(mer,(x-1)%taille,(y-1)%taille) :
        liste.append(((x-1)%taille,(y-1)%taille))
    if est_libre(mer,(x)%taille,(y-1)%taille) :
        liste.append(((x)%taille,(y-1)%taille))
    if est_libre(mer,(x+1)%taille,(y-1)%taille) :
        liste.append(((x+1)%taille,(y-1)%taille))
    return liste

def choix_cases_voisines_libres(mer,x,y):
    liste=liste_cases_voisines_libre(mer,x,y)
    if liste != []:
        random.shuffle(liste)
        return liste[0]
    else:
        return []

def est_thon(mer,x,y):
    return mer[y][x][0][0]==1

def est_requin(mer,x,y):
    return mer[y][x][1][0]==1

def deplacer_thon(mer,x,y):
    case_libre=choix_cases_voisines_libres(mer,x,y)
    if case_libre !=[]:
        #deplacement
        mer[case_libre[1]][case_libre[0]][0][0]=1
        mer[case_libre[1]][case_libre[0]][0][1]=mer[y][x][0][1]-1
        mer[y][x][0][0]=0
        mer[y][x][0][1]=gestation_thon
        if  mer[case_libre[1]][case_libre[0]][0][1]==0:
            mer[y][x][0][0]=1
            mer[y][x][0][1]=gestation_thon
            mer[case_libre[1]][case_libre[0]][0][1]=gestation_thon
    else:
        mer[y][x][0][1]=mer[y][x][0][1]-1
        if  mer[y][x][0][1]==0:
            mer[y][x][0][1]=gestation_thon
    return mer


def liste_cases_voisines_thon(mer,x,y):
    liste=[]
    taille=len(mer)
    if est_thon(mer,(x-1)%taille,(y+1)%taille) :
        liste.append(((x-1)%taille,(y+1)%taille))
    if est_thon(mer,(x)%taille,(y+1)%taille) :
        liste.append(((x)%taille,(y+1)%taille))
    if est_thon(mer,(x+1)%taille,(y+1)%taille) :
        liste.append(((x+1)%taille,(y+1)%taille))
    if est_thon(mer,(x-1)%taille,(y)%taille) :
        liste.append(((x-1)%taille,(y)%taille))
    if est_thon(mer,(x+1)%taille,(y)%taille) :
        liste.append(((x+1)%taille,(y)%taille))
    if est_thon(mer,(x-1)%taille,(y-1)%taille) :
        liste.append(((x-1)%taille,(y-1)%taille))
    if est_thon(mer,(x)%taille,(y-1)%taille) :
        liste.append(((x)%taille,(y-1)%taille))
    if est_thon(mer,(x+1)%taille,(y-1)%taille) :
        liste.append(((x+1)%taille,(y-1)%taille))
    return liste
def choix_cases_voisines_thon(mer,x,y):
    liste=liste_cases_voisines_thon(mer,x,y)
    random.shuffle(liste)
    if liste != []:
        return liste[0]
    else:
        return []
    
def deplacer_requin(mer,x,y):
    mer[y][x][1][2]=mer[y][x][1][2]-1
    case_thon=choix_cases_voisines_thon(mer,x,y)
    if case_thon!=[]:
        #on tue le thon
        mer[case_thon[1]][case_thon[0]][0][0]=0 
        mer[case_thon[1]][case_thon[0]][0][1]=gestation_thon
        #deplacement
        mer[case_thon[1]][case_thon[0]][1][0]=1 #requin present
        mer[case_thon[1]][case_thon[0]][1][1]=mer[y][x][1][1]-1 #gestation
        mer[case_thon[1]][case_thon[0]][1][2]=energie #energie
        #reinitialisation_case
        mer[y][x][1][0]=0
        mer[y][x][1][1]=gestation_requin
        mer[y][x][1][2]=energie
        mer[y][x][0][0]=0
        mer[y][x][0][1]=gestation_thon
        #mort
        #reproduction
        if mer[case_thon[1]][case_thon[0]][1][1]==0:
            #le bébé requin
            mer[y][x][1][0]=1
            mer[y][x][1][1]=gestation_requin
            mer[y][x][1][2]=energie
            mer[case_thon[1]][case_thon[0]][1][1]=gestation_requin
    else:
        case_vide=choix_cases_voisines_libres(mer,x,y)
        if case_vide != []:
            #deplacement
            mer[case_vide[1]][case_vide[0]][1][0]=1 #requin present
            mer[case_vide[1]][case_vide[0]][1][1]=mer[y][x][1][1]-1 #gestation
            mer[case_vide[1]][case_vide[0]][1][2]=mer[y][x][1][2]#energie
            #reinitialisation_case
            mer[y][x][1][0]=0
            mer[y][x][1][1]=gestation_requin
            mer[y][x][1][2]=energie
            mer[y][x][0][0]=0
            mer[y][x][0][1]=gestation_thon
            #mort
            if mer[case_vide[1]][case_vide[0]][1][2]==0:
                mer[case_vide[1]][case_vide[0]][1][0]=0
                mer[case_vide[1]][case_vide[0]][1][1]=gestation_requin
                mer[case_vide[1]][case_vide[0]][1][2]=energie
                #reproduction
            else:
                if mer[case_vide[1]][case_vide[0]][1][1]==0:
                    mer[y][x][1][0]=1
                    mer[y][x][1][1]=gestation_requin
                    mer[y][x][1][2]=energie
                    mer[case_vide[1]][case_vide[0]][1][1]=gestation_requin
        else:
            mer[y][x][1][1]=mer[y][x][1][1]-1
            if mer[y][x][1][1]==0:
                mer[y][x][1][1]=gestation_requin
    return mer

def nb_thon(mer):
    thon=0
    taille=len(mer)
    for i in range(taille):
        for j in range(taille):
            thon=thon+mer[i][j][0][0]
    return thon

def nb_requin(mer):
    thon=0
    taille=len(mer)
    for i in range(taille):
        for j in range(taille):
            thon=thon+mer[i][j][1][0]
    return thon

def nb_vide(mer):
    thon=0
    taille=len(mer)
    for i in range(taille):
        for j in range(taille):
            if mer[i][j][0][0]==0 and mer[i][j][1][0]==0:
                thon=thon+1
    return thon

taille=100
mer=remplissage_mer(taille,initialisation_thon,initialisation_requin)
T=[nb_thon(mer)/(taille*taille*1.0)*100.0]
R=[nb_requin(mer)/(taille*taille*1.0)*100.0]
V=[nb_vide(mer)]
A=[0]
for j in range(10000):
    for i in range(500):
        nb1=random.randint(0,taille-1)
        nb2=random.randint(0,taille-1)
        if est_thon(mer,nb1,nb2):
            deplacer_thon(mer,nb1,nb2)
        if est_requin(mer,nb1,nb2):
            deplacer_requin(mer,nb1,nb2)
    T.append(nb_thon(mer)/(taille*taille*1.0)*100.0)
    R.append(nb_requin(mer)/(taille*taille*1.0)*100.0)
    A.append(j*i)

    
"""
print(T)
print(R)
print(V)
"""


pylab.plot(A, R)
pylab.plot(A, T)
pylab.show()
pylab.plot(R, T)
pylab.show()
'''

visuel_mer=creer_mer(taille)
taille=len(mer)
for i in range(taille):
    for j in range(taille):
         if mer[j][i][0][0]==1:
             visuel_mer[j][i]="T"+str(mer[j][i][0][1])
         if mer[j][i][1][0]==1:
             visuel_mer[j][i]="R"+str(mer[j][i][1][1])

print(visuel_mer)
print(nb_thon(mer))
'''

