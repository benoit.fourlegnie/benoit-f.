from timeit import timeit
from tris import *
from listes import *
import pylab


def temps_tri_select(longueur_max):
    liste_temps=[]
    for taille in range(1,longueur_max+1):
        liste_temps.append(timeit(setup='from tris import tri_select; from listes import cree_liste_melangee',
       stmt='tri_select(cree_liste_melangee('+ str(taille) + '))',
       number=100))
    return liste_temps


def courbe_tri_select(longueur_max):
    '''
    tri_fusion
    tri_select
    tri_rapide
    '''
    x=range(1,longueur_max+1)
    y=temps_tri_select(longueur_max)
    NBRE_ESSAIS = 100
    pylab.title('Temps du tri par sélection (pour {:d} essais)'.format(NBRE_ESSAIS))
    pylab.xlabel('taille des listes')
    pylab.ylabel('temps en secondes')
    pylab.plot(x,y)
    pylab.grid()
    pylab.show()



def temps_tri(type_tri,longueur_max):
    liste_temps=[]
    for taille in range(1,longueur_max+1):
        liste_temps.append(timeit(setup='from tris import '+ type_tri +'; from listes import cree_liste_melangee', stmt= type_tri +'(cree_liste_melangee('+ str(taille) + '))', number=100))
    return liste_temps



def courbe_temps_tri(type_tri,longueur_max):
    '''
    tri_fusion
    tri_select
    tri_rapide
    '''
    x=range(1,longueur_max+1)
    y=temps_tri(type_tri,longueur_max)
    NBRE_ESSAIS = 100
    pylab.title('Temps du' +  type_tri  +'(pour {:d} essais)'.format(NBRE_ESSAIS))
    pylab.xlabel('taille des listes')
    pylab.ylabel('temps en secondes')
    pylab.plot(x,y)
    pylab.grid()
    pylab.show()
    
    
def courbe_temps(longueur_max):
    '''
    tri_fusion
    tri_select
    tri_rapide
    '''
    x=range(1,longueur_max+1)
    y1=temps_tri('tri_select',longueur_max)
    y2=temps_tri('tri_rapide',longueur_max)
    y3=temps_tri('tri_fusion',longueur_max)
    NBRE_ESSAIS = 100
    pylab.title('Temps du (pour {:d} essais)'.format(NBRE_ESSAIS))
    pylab.xlabel('taille des listes')
    #pylab.y1label('select')
    #pylab.y2label('rapide')
    #pylab.y3label('fusion')
    pylab.plot(x,y1)
    pylab.plot(x,y2)
    pylab.plot(x,y3)
    pylab.grid()
    pylab.show()